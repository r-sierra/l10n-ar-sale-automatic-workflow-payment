# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import fields, models


class AutomaticWorkflowJob(models.Model):
    """ Scheduler that will play automatically the validation of
    invoices, pickings...  """

    _inherit = "automatic.workflow.job"

    def _prepare_dict_account_payment(self, invoice):
        invoice = invoice.with_context(force_company=invoice.company_id.id)
        partner_type = (
            invoice.type in ("out_invoice", "out_refund") and "customer" or "supplier"
        )
        communication = (
            invoice.name
            if invoice.type in ("out_invoice", "out_refund")
            else invoice.reference
        )
        payment_type = 'inbound' if invoice.amount_residual > 0 else 'outbound'
        workflow_id = invoice.workflow_process_id
        journal_id = workflow_id.property_payment_journal_id
        payment_methods = payment_type == 'inbound' and journal_id.inbound_payment_method_ids or journal_id.outbound_payment_method_ids

        lines = invoice.mapped('line_ids').filtered(
            lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable')
        )
        debit_lines = [(0, 0, {
            'move_line_id': line.id,
            'amount': abs(line.amount_residual),
            'concile': True,
        }) for line in lines]

        vals = {
            "invoice_ids": [(6, 0, invoice.ids)],
            "amount": abs(invoice.amount_residual),
            "payment_date": fields.Date.context_today(self),
            "communication": communication,
            "currency_id": invoice.currency_id.id,
            "partner_id": invoice.partner_id.id,
            "partner_type": partner_type,
            'payment_type': payment_type,
            "journal_id": journal_id.id,
            "payment_method_id": payment_methods and payment_methods[0].id or False,
            "payment_imputation_ids": debit_lines,
        }

        if workflow_id.multiple_payment_journal:
            vals["payment_type_line_ids"] = [(0, 0, {
                "journal_id": workflow_id.property_payment_type_journal_id.id,
                "currency_id": invoice.currency_id.id,
                "amount": abs(invoice.amount_residual),
                "payment_currency_amount": abs(invoice.amount_residual),
            })]

        return vals
