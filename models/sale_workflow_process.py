# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models


class SaleWorkflowProcess(models.Model):

    _inherit = "sale.workflow.process"

    property_payment_journal_id = fields.Many2one(
        comodel_name="account.journal",
        company_dependent=True,
        string="Payment Journal",
        help="Set default journal to use on payment",
        domain="['&', ('type', 'in', ('bank', 'cash')), '|', ('at_least_one_inbound', '=', True), ('at_least_one_outbound', '=', True)]"
    )
    property_payment_type_journal_id = fields.Many2one(
        comodel_name="account.journal",
        company_dependent=True,
        string="Payment Method",
        help="Set the payment method to use on payment lines",
        domain="[('type', 'in', ('bank', 'cash')), ('multiple_payment_journal', '=', False), ('payment_method_of_multiple_payment', '=', True)]"
    )
    multiple_payment_journal = fields.Boolean(
        related='property_payment_journal_id.multiple_payment_journal',
        compute_sudo=False,
    )
    payment_filter_id = fields.Many2one(
        default=lambda self: self._default_filter(
            "l10n_ar_sale_automatic_workflow_payment.automatic_workflow_register_payment_filter"
        )
    )
