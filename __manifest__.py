# -*- coding: utf-8 -*-
{
    'name': 'Argentina - Sale Automatic Workflow Payments',
    'summary': 'Ajusta los pagos automáticos para múltiples métodos de pago',
    'author': 'Roberto Sierra <roberto@ideadigital.com.ar>',
    'website': 'https://gitlab.com/r-sierra/l10n-ar-sale-automatic-workflow-payment',
    'category': 'Sales Management',
    'license': 'AGPL-3',
    'version': '13.0.0.1.0',
    'depends': [
        'sale_automatic_workflow',
        'l10n_account_payment',
        'payment_imputation',
    ],
    'data': [
        'views/sale_workflow_process_view.xml',
        'data/automatic_workflow_data.xml',
    ],
}
